// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::cache::Cache;
use crate::dump::Dump;
use crate::sync_cache::SyncCache;
use serde::de::Deserializer;
use serde::ser::Serializer;
use serde::{Deserialize, Serialize};
use std::hash::Hash;

#[derive(Serialize, Deserialize)]
struct SerdeDump<K, V> {
    capacity: usize,
    data: Vec<(K, V)>,
}

// https://serde.rs/impl-serialize.html
impl<K, V> Serialize for Cache<K, V>
where
    K: Serialize + Eq + Hash + Clone,
    V: Serialize + Clone,
{
    /// Serialize the cache.
    ///
    /// This is possibly long, and requires data to support Clone.
    /// Internally, just does a Dump and serializes it.
    ///
    /// ```
    /// use hashlru::Cache;
    /// use serde_json::json;
    ///
    /// let mut cache = Cache::new(10);
    ///
    /// cache.insert(1, 10);
    /// cache.insert(2, 20);
    ///
    /// let export = json!(&cache).to_string();
    ///
    /// assert_eq!("{\"capacity\":10,\"data\":[[1,10],[2,20]]}", export);
    /// ```
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        self.dump().serialize(serializer)
    }
}

impl<K, V> Serialize for SyncCache<K, V>
where
    K: Serialize + Eq + Hash + Clone,
    V: Serialize + Clone,
{
    /// Serialize the sync cache.
    ///
    /// This is possibly long, and requires data to support Clone.
    /// Internally, just does a Dump and serializes it.
    ///
    /// ```
    /// use hashlru::SyncCache;
    /// use serde_json::json;
    ///
    /// let cache = SyncCache::new(10);
    ///
    /// cache.insert(1, 10);
    /// cache.insert(2, 20);
    ///
    /// let export = json!(&cache).to_string();
    ///
    /// assert_eq!("{\"capacity\":10,\"data\":[[1,10],[2,20]]}", export);
    /// ```
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        self.dump().serialize(serializer)
    }
}

// https://serde.rs/deserialize-struct.html
impl<'de, K, V> Deserialize<'de> for Cache<K, V>
where
    K: 'de + Eq + Hash + Deserialize<'de> + Clone,
    V: 'de + Deserialize<'de> + Clone,
{
    /// Deserialize the cache.
    ///
    /// Cache and SyncCache and Dump all share the same representation
    /// so it is fine to (de)serialize from any of those.
    ///
    /// ```
    /// use hashlru::Cache;
    ///
    /// let export = "{\"capacity\":7,\"data\":[[1,10],[4,40],[2,20]]}";
    ///
    /// let mut cache: Cache<usize, usize> = serde_json::from_str(&export).unwrap();
    ///
    /// assert_eq!(3, cache.len());
    /// assert_eq!(7, cache.capacity());
    /// assert_eq!(Some(&10), cache.get(&1));
    /// assert_eq!(Some(&40), cache.get(&4));
    /// assert_eq!(Some(&20), cache.get(&2));
    /// assert_eq!(Some(&1), cache.lru());
    /// assert_eq!(Some(&2), cache.mru());
    /// assert_eq!("[1: 10, 4: 40, 2: 20]", format!("{}", &cache));
    /// ```
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let dump: Dump<K, V> = Dump::deserialize(deserializer)?;
        Ok(Cache::<K, V>::from(dump))
    }
}

impl<'de, K, V> Deserialize<'de> for SyncCache<K, V>
where
    K: 'de + Eq + Hash + Deserialize<'de> + Clone,
    V: 'de + Deserialize<'de> + Clone,
{
    /// Deserialize the cache.
    ///
    /// Cache and SyncCache and Dump all share the same representation
    /// so it is fine to (de)serialize from any of those.
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let export = "{\"capacity\":7,\"data\":[[1,10],[4,40],[2,20]]}";
    ///
    /// let cache: SyncCache<usize, usize> = serde_json::from_str(&export).unwrap();
    ///
    /// assert_eq!(3, cache.len());
    /// assert_eq!(7, cache.capacity());
    /// assert_eq!(Some(10), cache.get(&1));
    /// assert_eq!(Some(40), cache.get(&4));
    /// assert_eq!(Some(20), cache.get(&2));
    /// assert_eq!(Some(1), cache.lru());
    /// assert_eq!(Some(2), cache.mru());
    /// assert_eq!("[sync] [1: 10, 4: 40, 2: 20]", format!("{}", &cache));
    /// ```
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let dump: Dump<K, V> = Dump::deserialize(deserializer)?;
        Ok(SyncCache::<K, V>::from(dump))
    }
}

#[cfg(test)]
mod tests {
    #[cfg(feature = "serde")]
    #[test]
    fn test_serialize_json() {
        use super::Cache;
        use serde_json::json;

        let mut cache = Cache::new(10);

        cache.insert(1, 10);
        cache.insert(2, 20);

        let export = json!(&cache).to_string();

        assert_eq!("{\"capacity\":10,\"data\":[[1,10],[2,20]]}", export);
    }

    #[test]
    fn test_deserialize_json() {
        use super::Cache;

        let export = "{\"capacity\":7,\"data\":[[1,10],[4,40],[2,20]]}";

        let mut cache: Cache<usize, usize> = serde_json::from_str(&export).unwrap();

        assert_eq!(3, cache.len());
        assert_eq!(7, cache.capacity());
        assert_eq!(Some(&10), cache.get(&1));
        assert_eq!(Some(&40), cache.get(&4));
        assert_eq!(Some(&20), cache.get(&2));
        assert_eq!(Some(&1), cache.lru());
        assert_eq!(Some(&2), cache.mru());
        assert_eq!("[1: 10, 4: 40, 2: 20]", format!("{}", &cache));
    }

    #[test]
    fn test_json() {
        use super::Cache;
        use serde_json::json;

        let mut cache1 = Cache::new(10);

        cache1.insert(1, 10);
        cache1.insert(2, 20);

        let export = json!(&cache1).to_string();

        assert_eq!("{\"capacity\":10,\"data\":[[1,10],[2,20]]}", export);

        let cache2: Cache<usize, usize> = serde_json::from_str(&export).unwrap();

        assert_eq!(&cache1, &cache2);
    }

    #[test]
    fn test_yaml() {
        use super::Cache;
        use serde_yaml::{from_str, to_string};

        let mut cache1 = Cache::new(10);

        cache1.insert(1, 10);
        cache1.insert(2, 20);

        let export = to_string(&cache1).unwrap();

        assert_eq!(
            "capacity: 10\ndata:\n- - 1\n  - 10\n- - 2\n  - 20\n",
            export
        );

        let cache2: Cache<usize, usize> = from_str(&export).unwrap();

        assert_eq!(&cache1, &cache2);
    }

    #[test]
    fn test_rmp() {
        use super::Cache;
        use rmp_serde::decode::from_slice;
        use rmp_serde::encode::to_vec;

        let mut cache1 = Cache::new(10);

        cache1.insert(1, 10);
        cache1.insert(2, 20);

        let export = to_vec(&cache1).unwrap();

        let cache2: Cache<usize, usize> = from_slice(&export[..]).unwrap();

        assert_eq!(&cache1, &cache2);
    }

    #[test]
    fn test_postcard() {
        extern crate alloc;
        use super::Cache;
        use alloc::vec::Vec;
        use postcard::{from_bytes, to_allocvec};

        let mut cache1 = Cache::<usize, usize>::new(1_000);

        for i in 0..cache1.capacity() / 2 {
            cache1.insert(i, i * 10);
        }

        let export: Vec<u8> = to_allocvec(&cache1).unwrap();
        println!("export len: {}", export.len());
        let cache2: Cache<usize, usize> = from_bytes(&export).unwrap();

        println!("cache1 capacity: {}", cache1.capacity());
        println!("cache2 capacity: {}", cache2.capacity());
        assert_eq!(&cache1, &cache2);
    }
}
