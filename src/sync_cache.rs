// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::cache::*;
use crate::dump::*;
use std::collections::HashMap;
use std::fmt;
use std::hash::Hash;
use std::sync::{Arc, RwLock};

/// Thread-safe LRU cache.
///
/// This is similar to the standard, non thread-safe version,
/// with the following differences:
///
/// - thread-safe, it is safe to call concurrently
/// - get/peek return copies, so keys and values need to implement Clone
/// - no iterators, only flat dumps to export data
/// - no Serde support, use dumps to dump/restore from cold state
///
/// # Examples
///
/// ```
/// use hashlru::SyncCache;
///
/// // Does not need to be `mut`
/// let cache = SyncCache::new(4);
/// cache.insert("key1", 10);
/// cache.insert("key2", 20);
/// cache.insert("key3", 30);
/// cache.insert("key4", 40);
/// cache.insert("key5", 50);
/// // key1 has been dropped, size is limited to 4
/// assert_eq!(Some("key2"), cache.lru());
/// assert_eq!(Some(20), cache.get(&"key2"));
/// // getting key2 has made key3 the least recently used item
/// assert_eq!(Some("key3"), cache.lru());
/// assert_eq!(Some(40), cache.get(&"key4"));
/// // getting key4 makes it the most recently used item
/// assert_eq!("[sync] [key3: 30, key5: 50, key2: 20, key4: 40]", format!("{}", cache));
/// ```
#[derive(Debug, Clone)]
pub struct SyncCache<K, V>
where
    K: Eq + Hash + Clone,
    V: Clone,
{
    inner: Arc<RwLock<Cache<K, V>>>,
}

/// Pretty-print cache content.
///
/// Prints key-value pairs as if it was an ordered list.
/// Which is, what it is, conceptually, even if implementation
/// details differ and there is no array backing the store.
///
/// # Examples
///
/// ```
/// use hashlru::SyncCache;
///
/// let a = SyncCache::new(900);
/// a.insert(1, "a");
/// a.insert(2, "b");
/// a.insert(3, "c");
/// assert_eq!("[sync] [1: a, 2: b, 3: c]", format!("{}", a));
/// for i in 10..1000 {
///      a.insert(i, "more");
/// }
/// // If there are too many keys, just print a few.
/// assert_eq!("[sync] [100: more, 101: more, ..., 999: more]", format!("{}", a));
/// ```
impl<K, V> fmt::Display for SyncCache<K, V>
where
    K: fmt::Display + Hash + Eq + Clone,
    V: fmt::Display + Clone,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[sync] {}", self.inner.read().unwrap())
    }
}

/// Create a thread-safe cache from an ordinary cache.
///
/// # Examples
///
/// ```
/// use hashlru::{Cache, SyncCache};
///
/// let a: Cache<String, String> = Cache::new(100);
/// let b = SyncCache::from(a);
/// assert_eq!(100, b.capacity());
/// ```
impl<K, V> From<Cache<K, V>> for SyncCache<K, V>
where
    K: Hash + Eq + Clone,
    V: Clone,
{
    fn from(cache: Cache<K, V>) -> SyncCache<K, V> {
        SyncCache {
            inner: Arc::new(RwLock::new(cache)),
        }
    }
}

/// Create an ordinary cache from a thread-safe cache.
///
/// This is possibly slow as it is O(n) since it clones the cache.
///
/// # Examples
///
/// ```
/// use hashlru::{Cache, SyncCache};
///
/// let a: SyncCache<String, String> = SyncCache::new(100);
/// let b = Cache::from(a);
/// assert_eq!(100, b.capacity());
/// ```
impl<K, V> From<SyncCache<K, V>> for Cache<K, V>
where
    K: Hash + Eq + Clone,
    V: Clone,
{
    fn from(cache: SyncCache<K, V>) -> Cache<K, V> {
        cache.inner.read().unwrap().clone()
    }
}

impl<K, V> SyncCache<K, V>
where
    K: Eq + Hash + Clone,
    V: Clone,
{
    /// Returns the number of elements in the map.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let a = SyncCache::new(10);
    /// assert_eq!(a.len(), 0);
    /// a.insert(1, "a");
    /// assert_eq!(a.len(), 1);
    /// ```
    #[inline]
    pub fn len(&self) -> usize {
        self.inner.read().unwrap().len()
    }

    /// Returns the max number of elements in the map.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let a: SyncCache::<u32,String> = SyncCache::new(10);
    /// assert_eq!(a.capacity(), 10);
    /// ```
    #[inline]
    pub fn capacity(&self) -> usize {
        self.inner.read().unwrap().capacity()
    }

    /// Returns true if cache is empty
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let a: SyncCache::<u32,String> = SyncCache::new(10);
    /// assert!(a.is_empty());
    /// a.insert(0, String::from("hello"));
    /// assert!(!a.is_empty());
    /// ```
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.inner.read().unwrap().is_empty()
    }

    /// Returns true if cache is full
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let a: SyncCache::<usize,usize> = SyncCache::new(10);
    /// assert!(!a.is_full());
    /// for i in 0..10 {
    ///     a.insert(i, i);
    /// }
    /// assert!(a.is_full());
    /// ```
    #[inline]
    pub fn is_full(&self) -> bool {
        self.inner.read().unwrap().is_full()
    }

    /// Clears the LRU cache, drops all data.
    /// Keeps the current capacity setting.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// lru.insert("x", 1);
    /// lru.insert("y", 10);
    ///
    /// lru.clear();
    /// assert_eq!(0, lru.len());
    /// assert_eq!(10, lru.capacity());
    /// ```
    #[inline]
    pub fn clear(&self) {
        self.inner.write().unwrap().clear()
    }

    /// Crate a new LRU. The `capacity` needs to be specified,
    /// as a 0-sized LRU can hold no data at all.
    ///
    /// It is possible to resize it later.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru: SyncCache<String, usize> = SyncCache::new(100);
    /// ```
    #[inline]
    pub fn new(capacity: usize) -> SyncCache<K, V> {
        Self::from(Cache::new(capacity))
    }

    /// Resizes the LRU cache. Drops least recently used entries
    /// in priority when size is reduced below current length.
    ///
    /// Returns the number of dropped entries, if any.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// lru.insert("x", 1);
    /// lru.insert("y", 10);
    /// lru.insert("z", 100);
    /// lru.insert("t", 1000);
    /// lru.insert("u", 10000);
    ///
    /// assert_eq!(2, lru.resize(3));
    /// assert_eq!(3, lru.len());
    /// assert_eq!(3, lru.capacity());
    /// ```
    #[inline]
    pub fn resize(&self, capacity: usize) -> usize {
        self.inner.write().unwrap().resize(capacity)
    }

    /// Inserts a key-value pair into the map.
    ///
    /// If the key already exists, returns the
    /// previous value for this key.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    /// let lru = SyncCache::new(10);
    ///
    /// assert_eq!(None, lru.insert(1, "a"));
    /// assert!(lru.contains_key(&1));
    /// assert!(!lru.contains_key(&2));
    /// assert_eq!(Some("a"), lru.insert(1, "b"));
    /// assert_eq!(Some("b"), lru.get(&1));
    /// ```
    #[inline]
    pub fn insert(&self, k: K, v: V) -> Option<V> {
        self.inner.write().unwrap().insert(k, v)
    }

    /// Pushes a key-value pair into the map.
    ///
    /// If one entry needs to be removed because the
    /// cache is full, return the entry that was removed.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    /// let lru = SyncCache::new(10);
    ///
    /// lru.push(1, "a");
    /// assert_eq!(lru.contains_key(&1), true);
    /// assert_eq!(lru.contains_key(&2), false);
    /// ```
    #[inline]
    pub fn push(&self, k: K, v: V) -> Option<(K, V)> {
        self.inner.write().unwrap().push(k, v)
    }

    /// Returns a reference to the value corresponding to the key.
    ///
    /// Since this is a LRU, reading alters the order of the items
    /// and will place this one first, as it is now the most recently used.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let cache = SyncCache::new(10);
    /// cache.insert(1, "a");
    /// cache.insert(2, "b");
    /// assert_eq!(Some(1), cache.lru());
    /// assert_eq!(cache.get(&1), Some("a"));
    /// assert_eq!(Some(2), cache.lru());
    /// assert_eq!(cache.get(&3), None);
    /// ```
    pub fn get(&self, k: &K) -> Option<V> {
        let mut cache = self.inner.write().unwrap();
        match cache.get(k) {
            Some(v) => Some(v.clone()),
            None => None,
        }
    }

    /// Bumps a key, making it the most recently used key (MRU).
    ///
    /// This is similar to doing a get() and ignore the value.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let cache = SyncCache::new(10);
    /// cache.insert(1, "a");
    /// cache.insert(2, "b");
    /// assert_eq!(Some(1), cache.lru());
    /// cache.bump(&1);
    /// assert_eq!(Some(2), cache.lru());
    /// ```
    #[inline]
    pub fn bump(&self, k: &K) {
        self.inner.write().unwrap().bump(k)
    }

    /// Returns a reference to the value corresponding to the key,
    /// along with the key itself.
    ///
    /// Since this is a LRU, reading alters the order of the items
    /// and will place this one first, as it is now the most recently used.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// lru.insert(1, "a");
    /// assert_eq!(Some((1, "a")), lru.get_key_value(&1));
    /// assert_eq!(None, lru.get(&2));
    /// ```
    pub fn get_key_value(&self, k: &K) -> Option<(K, V)> {
        let mut cache = self.inner.write().unwrap();
        match cache.get(k) {
            Some(v) => Some((k.clone(), v.clone())),
            None => None,
        }
    }

    /// Returns a reference to the value corresponding to the key.
    ///
    /// This is different from a standard get, it will not alter
    /// the order of items, and is a read-only operation.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// lru.insert(1, "a");
    /// assert_eq!(lru.peek(&1), Some("a"));
    /// assert_eq!(lru.peek(&2), None);
    /// ```
    pub fn peek(&self, k: &K) -> Option<V> {
        let cache = self.inner.write().unwrap();
        match cache.peek(k) {
            Some(v) => Some(v.clone()),
            None => None,
        }
    }

    /// Returns a reference to the value corresponding to the key,
    /// along with the key itself.
    ///
    /// This is different from a standard get_key_value, it will not alter
    /// the order of items, and is a read-only operation.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// lru.insert(1, "a");
    /// assert_eq!(Some((1, "a")), lru.peek_key_value(&1));
    /// assert_eq!(None, lru.peek(&2));
    /// ```
    pub fn peek_key_value(&self, k: &K) -> Option<(K, V)> {
        let cache = self.inner.write().unwrap();
        match cache.peek_key_value(k) {
            Some(v) => Some((v.0.clone(), v.1.clone())),
            None => None,
        }
    }

    /// Returns true if there is a value for the specified key.
    ///
    /// It does not alter the order of items, and is a read-only operation.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// lru.insert(421, true);
    /// assert!(lru.contains_key(&421));
    /// assert!(!lru.contains_key(&33));
    /// ```
    #[inline]
    pub fn contains_key(&self, k: &K) -> bool {
        self.inner.read().unwrap().contains_key(k)
    }

    /// Returns the least recently used key (LRU).
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// assert_eq!(None, lru.lru());
    /// lru.push("abc", true);
    /// assert_eq!(Some("abc"), lru.lru());
    /// lru.push("def", false);
    /// assert_eq!(Some("abc"), lru.lru());
    /// lru.get(&"abc");
    /// assert_eq!(Some("def"), lru.lru());
    /// ```
    #[inline]
    pub fn lru(&self) -> Option<K> {
        self.inner.read().unwrap().lru().cloned()
    }

    /// Pops the least recently used key, returning its value if
    /// the cache was non-empty.
    ///
    /// After it has been popped, it is no more in the cache.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// assert_eq!(None, lru.pop_lru());
    /// lru.push("abc", true);
    /// lru.push("def", false);
    /// assert_eq!(Some(("abc", true)), lru.pop_lru());
    /// assert_eq!(1, lru.len());
    /// ```
    #[inline]
    pub fn pop_lru(&self) -> Option<(K, V)> {
        self.inner.write().unwrap().pop_lru()
    }

    /// Gets the least recently used key, returning its value if
    /// the cache was non-empty.
    ///
    /// After it is returned, it becomes the most recently used
    /// item, so a next call to get_lru() would return a different value.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// assert_eq!(None, lru.pop_lru());
    /// lru.push("abc", true);
    /// lru.push("def", false);
    /// assert_eq!(Some(("abc", true)), lru.get_lru());
    /// assert_eq!(2, lru.len());
    /// assert_eq!(Some("def"), lru.lru());
    /// ```
    pub fn get_lru(&self) -> Option<(K, V)> {
        let mut cache = self.inner.write().unwrap();
        match cache.get_lru() {
            Some(v) => Some((v.0.clone(), v.1.clone())),
            None => None,
        }
    }

    /// Peeks the least recently used key, returning its value if
    /// the cache was non-empty.
    ///
    /// This does not alter the order of the cache.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// assert_eq!(None, lru.peek_lru());
    /// lru.push("abc", true);
    /// lru.push("def", false);
    /// assert_eq!(Some(("abc", true)), lru.peek_lru());
    /// assert_eq!(2, lru.len());
    /// assert_eq!(Some("abc"), lru.lru());
    /// ```
    pub fn peek_lru(&self) -> Option<(K, V)> {
        let cache = self.inner.write().unwrap();
        match cache.peek_lru() {
            Some(v) => Some((v.0.clone(), v.1.clone())),
            None => None,
        }
    }

    /// Similar to mru() but returns the key, not only a reference to it.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// assert_eq!(None, lru.mru());
    /// lru.push("abc", true);
    /// assert_eq!(Some("abc"), lru.mru());
    /// lru.push("def", false);
    /// assert_eq!(Some("def"), lru.mru());
    /// lru.get(&"abc");
    /// assert_eq!(Some("abc"), lru.mru());
    /// ```
    #[inline]
    pub fn mru(&self) -> Option<K> {
        self.inner.read().unwrap().mru().cloned()
    }

    /// Pops the most recently used key, returning its value if
    /// the cache was non-empty.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// assert_eq!(None, lru.pop_mru());
    /// lru.push("abc", true);
    /// lru.push("def", false);
    /// assert_eq!(Some(("def", false)), lru.pop_mru());
    /// assert_eq!(1, lru.len());
    /// ```
    #[inline]
    pub fn pop_mru(&self) -> Option<(K, V)> {
        self.inner.write().unwrap().pop_mru()
    }

    /// Peeks the most recently used key.
    /// Returns its key and value.
    ///
    /// There is no need for a get_mru, as getting the
    /// most recently used key does not alter order,
    /// so peek and get are equivalent.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// assert_eq!(None, lru.peek_mru());
    /// lru.push("abc", true);
    /// lru.push("def", false);
    /// assert_eq!(Some(("def", false)), lru.peek_mru());
    /// assert_eq!(2, lru.len());
    /// assert_eq!(Some("def"), lru.mru());
    /// ```
    pub fn peek_mru(&self) -> Option<(K, V)> {
        let cache = self.inner.write().unwrap();
        match cache.peek_mru() {
            Some(v) => Some((v.0.clone(), v.1.clone())),
            None => None,
        }
    }

    /// Removes a key, returning the (key,value) pair if there
    /// was already something for this key.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// lru.insert("abc", true);
    /// lru.insert("def", true);
    /// lru.insert("ghi", false);
    /// assert_eq!(Some(("def", true)), lru.pop(&"def"));
    /// assert!(lru.contains_key(&"abc"));
    /// assert!(!lru.contains_key(&"def"));
    /// assert!(lru.contains_key(&"ghi"));
    /// ```
    #[inline]
    pub fn pop(&self, k: &K) -> Option<(K, V)> {
        self.inner.write().unwrap().pop(k)
    }

    /// Removes a key, returning the value of the key if the
    /// key was previously in the map.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// lru.insert("abc", true);
    /// lru.insert("def", true);
    /// lru.insert("ghi", false);
    /// assert_eq!(Some(true), lru.remove(&"def"));
    /// assert!(lru.contains_key(&"abc"));
    /// assert!(!lru.contains_key(&"def"));
    /// assert!(lru.contains_key(&"ghi"));
    /// ```
    #[inline]
    pub fn remove(&self, k: &K) -> Option<V> {
        self.inner.write().unwrap().remove(k)
    }

    /// Delete a key, return nothing, just ensure key is gone.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let lru = SyncCache::new(10);
    /// lru.insert("abc", true);
    /// lru.insert("def", true);
    /// lru.insert("ghi", false);
    /// lru.delete(&"def");
    /// assert!(lru.contains_key(&"abc"));
    /// assert!(!lru.contains_key(&"def"));
    /// assert!(lru.contains_key(&"ghi"));
    /// ```
    #[inline]
    pub fn delete(&self, k: &K) {
        self.inner.write().unwrap().delete(k)
    }
}

impl<K, V> FromIterator<(K, V)> for SyncCache<K, V>
where
    K: Eq + Hash + Clone,
    V: Clone,
{
    /// Creates a new cache from an iterator.
    ///
    /// With this, you can use collect() to build a cache.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    /// use std::collections::HashMap;
    ///
    /// let mut src: HashMap<usize, &str> = HashMap::new();
    /// src.insert(1, "two");
    /// src.insert(2, "four");
    /// src.insert(3, "eight");
    ///
    /// let cache = src.into_iter().filter(|x| x.0 != 2).collect::<SyncCache<usize, &str>>();
    /// assert_eq!(2, cache.len());
    /// assert_eq!(Some("two"), cache.get(&1));
    /// assert_eq!(Some("eight"), cache.get(&3));
    /// ```
    fn from_iter<I: IntoIterator<Item = (K, V)>>(iter: I) -> Self {
        let c = SyncCache::new(0);

        for i in iter {
            c.resize(c.len() + 1);
            c.push(i.0, i.1);
        }

        c
    }
}

impl<K, V> SyncCache<K, V>
where
    K: Hash + Eq + Clone,
    V: Clone,
{
    /// Creates a dump of all keys and values, provided
    /// both have the Clone trait.
    ///
    /// The dump has a complete copy of all data, with ownership.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::{SyncCache, Dump};
    ///
    /// let cache = SyncCache::new(10);
    /// cache.insert("x", 1);
    /// cache.insert("y", 10);
    /// cache.insert("z", 100);
    ///
    /// let dump = cache.dump();
    /// assert_eq!("Dump { capacity: 10, data: [(\"x\", 1), (\"y\", 10), (\"z\", 100)] }", format!("{:?}", &dump));
    /// ```
    pub fn dump(&self) -> Dump<K, V> {
        self.inner.read().unwrap().dump()
    }

    /// Restores data from a dump. Clears the data before
    /// importing the dump.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::{SyncCache, Dump};
    ///
    /// let dump = Dump {
    ///     capacity: 10,
    ///     data: vec![("x", 1), ("y", 10), ("z", 100)],
    /// };
    /// let cache = SyncCache::new(2);
    /// assert_eq!(2, cache.capacity());
    /// cache.insert("a", 0);
    /// cache.insert("b", 1);
    /// cache.insert("c", 2);
    /// assert_eq!(2, cache.len());
    /// assert_eq!(3, cache.restore(&dump));
    /// assert_eq!(10, cache.capacity());
    /// assert_eq!("[sync] [x: 1, y: 10, z: 100]", format!("{}", &cache));
    /// // another way to restore, taking ownership
    /// let other_cache = SyncCache::from(dump);
    /// ```
    pub fn restore(&self, dump: &Dump<K, V>) -> usize {
        self.inner.write().unwrap().restore(dump)
    }

    /// Import data from an iterator, typically the iterator returned by into_iter().
    ///
    /// The data is not cleared before import, so values add to the existing ones.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::{Cache, SyncCache};
    ///
    /// let mut cache1 = Cache::new(5);
    /// cache1.insert("a", 0);
    /// cache1.insert("b", 1);
    /// cache1.insert("c", 2);
    /// let mut cache2 = SyncCache::new(10);
    /// cache2.insert("d", 3);
    /// cache2.import(cache1.into_iter());
    /// assert_eq!(4, cache2.len());
    /// assert_eq!("[sync] [d: 3, a: 0, b: 1, c: 2]", format!("{}", &cache2));
    /// ```
    pub fn import<I>(&mut self, iter: I) -> usize
    where
        I: Iterator<Item = (K, V)>,
    {
        for i in iter {
            self.insert(i.0, i.1);
        }
        self.len()
    }

    /// Exports all items as a map.
    ///
    /// Does not preserve order.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let cache = SyncCache::new(10);
    /// cache.insert("x", 1);
    /// cache.insert("y", 10);
    /// cache.insert("z", 100);
    ///
    /// let map = cache.to_map();
    /// assert_eq!(3, map.len());
    /// assert_eq!(Some(&1), map.get(&"x"));
    /// assert_eq!(Some(&10), map.get(&"y"));
    /// assert_eq!(Some(&100), map.get(&"z"));
    /// ```
    pub fn to_map(&self) -> HashMap<K, V> {
        self.inner.read().unwrap().to_map()
    }

    /// Exports all items as a vec.
    ///
    /// Preserves order, LRU has index 0.
    ///
    /// # Examples
    ///
    /// ```
    /// use hashlru::SyncCache;
    ///
    /// let cache = SyncCache::new(10);
    /// cache.insert("x", 1);
    /// cache.insert("y", 10);
    /// cache.insert("z", 100);
    ///
    /// let vec = cache.to_vec();
    /// assert_eq!(vec![("x", 1), ("y", 10), ("z", 100)], vec);
    /// ```
    pub fn to_vec(&self) -> Vec<(K, V)> {
        self.inner.read().unwrap().to_vec()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::Rng;
    use std::fmt::Debug;
    use std::thread;

    #[test]
    fn test_debug() {
        let lru = SyncCache::<i32, &str>::new(7);
        lru.insert(1, "abc");
        lru.insert(2, "def");
        lru.insert(3, "fgh");

        let debug = format!("{:?}", lru);
        assert!(debug.starts_with("SyncCache { inner"));
    }

    #[test]
    fn test_display() {
        let lru = SyncCache::<i32, &str>::new(7);
        lru.insert(1, "abc");
        lru.insert(2, "def");
        lru.insert(3, "fgh");

        let display = format!("{}", lru);
        assert_eq!("[sync] [1: abc, 2: def, 3: fgh]", display);
    }

    #[test]
    fn test_standard_map() {
        let lru = SyncCache::new(1000); // 1000 means no limit, for the sake of this test
        assert_eq!(None, lru.insert(1, "one"));
        assert_eq!(None, lru.insert(2, "two"));
        assert_eq!(None, lru.insert(3, "three"));
        assert_eq!(3, lru.len());
        assert_eq!(Some("one"), lru.insert(1, "ONE"));
        assert_eq!(None, lru.remove(&4));
        assert_eq!(Some("ONE"), lru.remove(&1));
        assert_eq!(Some("two"), lru.remove(&2));
        assert_eq!(Some("three"), lru.remove(&3));
        assert_eq!(None, lru.remove(&1));
    }

    #[test]
    fn test_pop() {
        let cache: SyncCache<usize, usize> = SyncCache::new(10);
        assert_eq!(None, cache.insert(1, 10));
        assert_eq!(None, cache.insert(2, 20));
        assert_eq!(None, cache.insert(3, 30));
        assert_eq!(10, cache.capacity());
        assert_eq!(3, cache.len());
        assert_eq!(Some((2, 20)), cache.pop(&2));
        assert_eq!(None, cache.pop(&2));
        assert_eq!(Some(1), cache.lru());
        assert_eq!(Some(3), cache.mru());
        assert_eq!(Some((1, 10)), cache.pop(&1));
        assert_eq!(Some((3, 30)), cache.pop(&3));
        assert_eq!(0, cache.len());
        assert_eq!(None, cache.pop(&3));
    }

    #[test]
    fn test_lru0() {
        let lru = SyncCache::new(0);
        assert_eq!(0, lru.len());
        assert_eq!(0, lru.capacity());
        assert_eq!(None, lru.insert(1, "one"));
        assert_eq!(0, lru.len());
        assert_eq!(0, lru.capacity());
        assert_eq!(None, lru.pop_lru());
        assert_eq!(None, lru.remove(&1));
        assert_eq!("[sync] []", format!("{}", lru));
    }

    // #[test]
    // fn test_lru1() {
    //     let lru = SyncCache::new(1);
    //     assert_eq!(0, lru.len());
    //     assert_eq!(1, lru.capacity());
    //     assert_eq!(None, lru.push(1, "two"));
    //     assert_eq!(1, lru.len());
    //     assert_eq!(Some("two"), lru.peek(&1));
    //     assert_eq!(Some("two"), lru.get(&1));
    //     assert_eq!(None, lru.push(1, "one")); // nothing returned because we push same key
    //     assert_eq!(Some("one"), lru.insert(1, "ONE")); // previous value for one returned
    //     assert_eq!(Some((1, "ONE")), lru.push(2, "two")); // lru value returned because cache full
    //     assert_eq!(None, lru.insert(1, "one")); // nothing returned because we push new key
    //     assert_eq!(1, lru.len());
    //     assert_eq!("[sync] [1: one]", format!("{}", lru));
    // }

    #[test]
    fn test_lru5() {
        let lru = SyncCache::new(5);
        assert_eq!(None, lru.insert(1, "one"));
        assert_eq!(None, lru.insert(2, "two"));
        assert_eq!(None, lru.insert(3, "three"));
        assert_eq!(Some("three"), lru.insert(3, "THREE"));
        assert_eq!(None, lru.insert(4, "four"));
        assert_eq!(None, lru.insert(5, "five"));
        assert_eq!(None, lru.insert(6, "six"));
        assert_eq!(5, lru.len());
        assert_eq!(None, lru.get(&1));
        assert_eq!(None, lru.insert(7, "seven"));
        assert_eq!(5, lru.len());
        assert_eq!(Some("seven"), lru.get(&7));
        assert_eq!(5, lru.len());
        assert_eq!(None, lru.get(&1));
        assert_eq!(None, lru.get(&2));
        assert_eq!(Some("THREE"), lru.get(&3));
        assert_eq!(Some("four"), lru.get(&4));
        assert_eq!(None, lru.insert(8, "eight"));
        assert_eq!(None, lru.insert(9, "nine"));
        assert_eq!(None, lru.insert(10, "ten"));
        assert_eq!(None, lru.insert(11, "eleven"));
        assert_eq!(5, lru.len());
        assert!(format!("{:?}", lru).starts_with("SyncCache { inner:",));
        assert_eq!(
            "[sync] [4: four, 8: eight, 9: nine, 10: ten, 11: eleven]",
            format!("{}", lru)
        );
        assert_eq!(Some("four"), lru.get(&4));
        assert_eq!(None, lru.get(&3));
        assert_eq!(Some("eight"), lru.remove(&8));
        assert_eq!(Some("nine"), lru.remove(&9));
        assert_eq!(Some("ten"), lru.remove(&10));
        assert_eq!(Some("eleven"), lru.remove(&11));
        assert_eq!(Some("four"), lru.remove(&4));
        assert_eq!(0, lru.len());
    }

    #[test]
    fn test_lru100() {
        let lru = SyncCache::new(100);
        for i in 0..1000 {
            lru.push(i, format!("value_{}", i));
        }
        assert_eq!(100, lru.len());
        assert!(format!("{:?}", lru).starts_with("SyncCache { inner:",));
        assert_eq!(
            "[sync] [900: value_900, 901: value_901, ..., 999: value_999]",
            format!("{}", lru)
        );

        assert_eq!(Some("value_950".to_string()), lru.get(&950));
        assert_eq!(
            Some((950, "value_950".to_string())),
            lru.get_key_value(&950)
        );
        assert_eq!(Some((950, "value_950".to_string())), lru.peek_mru());
        assert_eq!(Some(950), lru.mru());
        assert_eq!(Some((900, "value_900".to_string())), lru.get_lru());
        assert_eq!(Some((901, "value_901".to_string())), lru.get_lru());
        assert_eq!(Some((902, "value_902".to_string())), lru.peek_lru());
        assert_eq!(Some((902, "value_902".to_string())), lru.peek_lru());
        assert_eq!(
            "[sync] [902: value_902, 903: value_903, ..., 901: value_901]",
            format!("{}", lru)
        );
        assert_eq!(Some(902), lru.lru());
        assert_eq!(Some((901, "value_901".to_string())), lru.peek_mru());
        assert_eq!(Some(901), lru.mru());
    }

    #[derive(Debug, Eq, PartialEq, Clone)]
    struct Coord {
        x: i64,
        y: i64,
    }

    fn monkey_fn(cache: SyncCache<String, Coord>) {
        let mut rng = rand::thread_rng();
        for i in 0..1_000_000 {
            if i % 1000 == 0 {
                print!(".");
            }
            if i % 100_000 == 0 {
                cache.clear();
                continue;
            }
            let dice = rng.gen_range(0..=100);
            if dice < 20 {
                let key = format!("key_{}", i % 100);
                let value = Coord {
                    x: i % 89,
                    y: i % 97,
                };
                cache.insert(key, value);
                continue;
            }
            if dice < 40 {
                let key = format!("key_{}", i % 100);
                let value = Coord {
                    x: i % 89,
                    y: i % 97,
                };
                cache.push(key, value);
                continue;
            }
            if dice < 50 {
                let key = format!("key_{}", i % 10);
                cache.get(&key);
                continue;
            }
            if dice < 60 {
                let key = format!("key_{}", i % 10);
                cache.peek(&key);
                continue;
            }
            if dice < 70 {
                let key = format!("key_{}", i % 10);
                cache.pop(&key);
                continue;
            }
            if dice < 80 {
                let key = format!("key_{}", i % 10);
                cache.remove(&key);
                continue;
            }
            if dice < 81 {
                let dump = cache.dump();
                let other: SyncCache<String, Coord> = SyncCache::new(100);
                other.restore(&dump);
            }
        }
    }

    #[test]
    fn test_monkey() {
        let cache: SyncCache<String, Coord> = SyncCache::new(50);

        let cache1 = cache.clone();
        let handle1 = thread::spawn(move || monkey_fn(cache1));
        let cache2 = cache.clone();
        let handle2 = thread::spawn(move || monkey_fn(cache2));

        handle1.join().unwrap();
        handle2.join().unwrap();
    }
}
