#![feature(test)]
extern crate caches;
extern crate lru;
extern crate test;

#[cfg(test)]
mod tests {
    use super::*;
    use caches::lru::LRUCache;
    use caches::Cache as CachesCache;
    use hashlru::{Cache, SyncCache};
    use lru::LruCache;
    use std::collections::HashMap;
    use std::num::NonZeroUsize;
    use test::Bencher;

    const CAPACITY: usize = 100_000;

    #[bench]
    fn bench_write_usize_hashlru_cache(b: &mut Bencher) {
        let mut cache = Cache::new(CAPACITY);
        let mut i: isize = 0;
        b.iter(|| {
            cache.insert(i / 2, i);
            i += 1;
        });
    }

    #[bench]
    fn bench_write_usize_hashlru_sync_cache(b: &mut Bencher) {
        let cache = SyncCache::new(CAPACITY);
        let mut i: isize = 0;
        b.iter(|| {
            cache.insert(i / 2, i);
            i += 1;
        });
    }

    #[bench]
    fn bench_write_usize_builtin_hashmap(b: &mut Bencher) {
        let mut cache = HashMap::with_capacity(CAPACITY);
        let mut i: isize = 0;
        b.iter(|| {
            cache.insert(i / 2, i);
            i += 1;
        });
    }

    #[bench]
    fn bench_write_usize_extern_lru(b: &mut Bencher) {
        let mut cache = LruCache::new(NonZeroUsize::new(CAPACITY).unwrap());
        let mut i: isize = 0;
        b.iter(|| {
            cache.push(i / 2, i);
            i += 1;
        });
    }

    #[bench]
    fn bench_write_usize_extern_caches(b: &mut Bencher) {
        let mut cache = LRUCache::new(CAPACITY).unwrap();
        let mut i: isize = 0;
        b.iter(|| {
            cache.put(i / 2, i);
            i += 1;
        });
    }

    #[bench]
    fn bench_read_usize_hashlru_cache(b: &mut Bencher) {
        let mut cache = Cache::new(CAPACITY);
        for i in 0..CAPACITY {
            cache.insert(i, i);
        }
        let mut i: usize = 0;
        b.iter(|| {
            cache.get(&i);
            i = (i + 7) % CAPACITY;
        });
    }

    #[bench]
    fn bench_read_usize_hashlru_sync_cache(b: &mut Bencher) {
        let cache = SyncCache::new(CAPACITY);
        for i in 0..CAPACITY {
            cache.insert(i, i);
        }
        let mut i: usize = 0;
        b.iter(|| {
            cache.get(&i);
            i = (i + 7) % CAPACITY;
        });
    }

    #[bench]
    fn bench_read_usize_builtin_hashmap(b: &mut Bencher) {
        let mut cache = HashMap::with_capacity(CAPACITY);
        for i in 0..CAPACITY {
            cache.insert(i, i);
        }
        let mut i: usize = 0;
        b.iter(|| {
            cache.get(&i);
            i = (i + 7) % CAPACITY;
        });
    }

    #[bench]
    fn bench_read_usize_extern_lru(b: &mut Bencher) {
        let mut cache = LruCache::new(NonZeroUsize::new(CAPACITY).unwrap());
        for i in 0..CAPACITY {
            cache.push(i, i);
        }
        let mut i: usize = 0;
        b.iter(|| {
            cache.get(&i);
            i = (i + 7) % CAPACITY;
        });
    }

    #[bench]
    fn bench_read_usize_extern_caches(b: &mut Bencher) {
        let mut cache = LRUCache::new(CAPACITY).unwrap();
        for i in 0..CAPACITY {
            cache.put(i, i);
        }
        let mut i: usize = 0;
        b.iter(|| {
            cache.get(&i);
            i = (i + 7) % CAPACITY;
        });
    }
}
